#include "unity.h"
#include "hal_usart_02.h"
#include "stub.h"

HAL_StatusTypeDef Usart_status;

void setUp(void)
{
}

void tearDown(void)
{
}

void test_hal_usart_02_NeedToImplement(void)
{
    USART_HandleTypeDef *husart,Phusart,Testhusart,*TTesthusart;
	uint8_t VpRxData;
	uint8_t *pTxData;
	uint8_t *pRxData;
    uint32_t Timeout;
	uint16_t Size;
    int a[104];



    /****************************  Case 1   *********************************/
    Phusart.State = HAL_USART_STATE_READY;
    husart = &Phusart;
    pRxData = NULL;
    pTxData = NULL;
    Size = 0;
    Usart_status = HAL_USART_TransmitReceive(husart,pTxData,pRxData,Size,Timeout);

    /****************************  Case 2   *********************************/
    VpRxData = 0x08;
    pRxData = &VpRxData;
    pTxData = &VpRxData;
    Size = 16;
    Usart_status = HAL_USART_TransmitReceive(husart,pTxData,pRxData,Size,Timeout);
   
     /****************************  Case 3   *********************************/  
    Phusart.State = HAL_USART_STATE_READY;
    Phusart.Init.Parity = USART_PARITY_NONE;
    VpRxData = 0x08;
    pRxData = &VpRxData;
    pTxData = &VpRxData;
    Usart_status = HAL_USART_TransmitReceive(husart,pTxData,pRxData,Size,Timeout);

    /****************************  Case 4   *********************************/
    Phusart.Init.Parity = USART_PARITY_NONE;
    husart = &Phusart;
    VpRxData = 0x08;
    pRxData = &VpRxData;
    pTxData = &VpRxData;
    Usart_status = HAL_USART_TransmitReceive(husart,pTxData,pRxData,Size,Timeout);

    /****************************  Case 5   *********************************/
    Phusart.State = HAL_USART_STATE_READY;
    Phusart.Init.Parity = USART_PARITY_ODD;
    husart = &Phusart;
    VpRxData = 0x08;
    pRxData = &VpRxData;
    pTxData = &VpRxData;
    Size = 1;
    Usart_status = HAL_USART_TransmitReceive(husart,pTxData,pRxData,Size,Timeout);
 
     /****************************  Case 6   *********************************/   
    Phusart.State = HAL_USART_STATE_READY;
    Phusart.Init.Parity = USART_PARITY_NONE;
    VpRxData = 0x08;
    pRxData = &VpRxData;
    pTxData = &VpRxData;
    Size = 1;
    Usart_status = HAL_USART_TransmitReceive(husart,pTxData,pRxData,Size,Timeout);
    
    /****************************  Case 7   *********************************/    
    Phusart.Init.WordLength = USART_WORDLENGTH_9B;
    VpRxData = 0x08;
    pRxData = &VpRxData;
    pTxData = &VpRxData;
    Size = 1;
    Usart_status = HAL_USART_TransmitReceive(husart,pTxData,pRxData,Size,Timeout);

    /****************************  Case 8   *********************************/
    Phusart.Init.Parity = USART_PARITY_ODD;
    Phusart.Init.WordLength = USART_WORDLENGTH_9B; 
    VpRxData = 0x08;
    pRxData = &VpRxData;
    pTxData = &VpRxData;
    Size = 1;
    Usart_status = HAL_USART_TransmitReceive(husart,pTxData,pRxData,Size,Timeout);

    /****************************  Case 9   *********************************/
    Phusart.Init.Parity = USART_PARITY_ODD;
    Phusart.Init.WordLength = USART_WORDLENGTH_9B; 
    VpRxData = 0x08;
    pRxData = &VpRxData;
    pTxData = &VpRxData;
    Size = 1;
    Usart_status = HAL_USART_TransmitReceive(husart,pTxData,pRxData,Size,Timeout);

    /****************************  Case 10   *********************************/   
     Phusart.Init.Parity = USART_PARITY_ODD;
    Phusart.Init.WordLength = USART_WORDLENGTH_9B; 
    VpRxData = 0x08;
    pRxData = &VpRxData;
    pTxData = &VpRxData;
    Size = 1;
    Usart_status = HAL_USART_TransmitReceive(husart,pTxData,pRxData,Size,Timeout); 
 
     /****************************  Case 11   *********************************/   
    Phusart.State = HAL_USART_STATE_READY;
    Phusart.Init.WordLength = USART_WORDLENGTH_9B; 
    VpRxData = 0x08;
    pRxData = &VpRxData;
    pTxData = &VpRxData;
    Size = 1;
    Usart_status = HAL_USART_TransmitReceive(husart,pTxData,pRxData,Size,Timeout); 
}