#ifndef AMSTB_H
#define AMSTB_H

#ifndef DH_H
#define DH_H
#include "hal_usart_02.h"
#endif
uint32_t Value;

DMA_HandleTypeDef PHALlock;
extern  void AMSTB__HAL_LOCK(DMA_HandleTypeDef *AMSTB_DMAStart);

DMA_HandleTypeDef PHALunlock;
extern  void AMSTB__HAL_UNLOCK(DMA_HandleTypeDef *AMSTB_DMAStart);



extern HAL_StatusTypeDef USART_WaitOnFlagUntilTimeout(USART_HandleTypeDef *husart, uint32_t Flag, FlagStatus Status, uint32_t Tickstart, uint32_t Timeout);

#endif 